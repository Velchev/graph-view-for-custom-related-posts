=== Graph View For Related Posts ===
Contributors: Nikolay Velchev
Tags: graph view, related posts, manual related posts
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Generate view of posts that are related manually

== Description ==

This plugin extends "Custom related posts" plugin adding graph visualization of the related posts.

To use the plugin create new page and add [graph-view-custom-related-posts] tag and it will be replaced with the graph view of all pages that are using the "Custom related posts" plugin.

== Installation ==

1. Upload the `graph-view-for-custom-related-posts` directory (directory included) to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress