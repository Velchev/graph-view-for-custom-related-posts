# Graph view of custom related posts
This plugin extends "[Custom related posts](https://wordpress.org/plugins/custom-related-posts/)" plugin adding graph visualization of the
related posts.

To use the plugin create new page and add `[graph-view-custom-related-posts]` tag and it will be replaced with the graph view of all pages that are using the  "Custom related posts" plugin.

## Used technologies
 - PHP
 - JavaScript
 - D3 library https://d3js.org/d3-force
 - npm

## Screenshots
![example1.png](img%2Fexample1.png)
![example2.png](img%2Fexample2.png)
### Author
Nikolay Velchev - https://nvelchev.com