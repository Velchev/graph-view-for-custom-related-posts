<?php
/*
* Plugin Name: Graph view of custom related posts
* Plugin URI:  https://gitlab.com/Velchev/graph-view-for-custom-related-posts
* Description: Gives graph view of the pages linked with "Custom related posts" plugin. It's a plugin for a plugin : )
* Version:     1.0.0
* Author:      Nikolay Velchev
* Author URI:  https://nvelchev.com
* License:     GPL v2 or later
* License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

if (!defined('GVFCRP_VER'))
    define('GVFCRP_VER', '1.0.0');

// Start up the engine
class GraphViewForCustomRelatedPostsPlugin
{
    private static $CUSTOM_TAG = '[graph-view-custom-related-posts]';
    private static $HTML_TEMPLATE_JSON_PLACE_HOLDER = '"[replace-me-with-json-object-nodes-links]"';

    /**
     * Static property to hold our singleton instance
     *
     */
    static $instance = false;

    /**
     * This is our constructor
     *
     * @return void
     */
    private function __construct()
    {
        add_filter('the_content', array($this, 'custom_tag_replacement'));
    }

    public static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self;
        return self::$instance;
    }

    function custom_tag_replacement($content)
    {
        // Check if the post content contains the custom tag
        if (strpos($content, self::$CUSTOM_TAG) !== false) {


            $from = $this->get_meta_tags('crp_relations_from');
            $to = $this->get_meta_tags('crp_relations_to');

            foreach ($to['nodes'] as $toNode) {
                $toNodeId = $toNode['id'];

                $found = false;
                foreach ($from['nodes'] as $fromNode) {
                    if ($fromNode['id'] == $toNodeId) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $from['nodes'][] = $toNode;
                }
            }

            $from['links'] = array_merge($from['links'], $to['links']);

            $templatePath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'index.html';
            $htmlTemplate = file_get_contents($templatePath);
            $htmlTemplate = str_replace(self::$HTML_TEMPLATE_JSON_PLACE_HOLDER, json_encode($from), $htmlTemplate);
            $content = str_replace(self::$CUSTOM_TAG, $htmlTemplate, $content);

            $script_url = plugins_url('node_modules/d3/dist/d3.js', __FILE__);
            wp_enqueue_script('custom-script', $script_url);
        }

        return $content;
    }

    function get_meta_tags($meta_key)
    {
        global $wpdb;

        $query = $wpdb->prepare(
            "SELECT pm.post_id, pm.meta_value, p.post_title, p.guid
                        FROM {$wpdb->postmeta} pm 
                        LEFT JOIN {$wpdb->posts} p on p.ID = pm.post_id AND p.post_status = 'publish'
                        WHERE meta_key = %s",
            $meta_key
        );

        $results = $wpdb->get_results($query);

        $nodes = array();
        $links = array();

        $addedNodes = array();

        if ($results) {
            foreach ($results as $result) {
                $post_id = (int)$result->post_id;
                $meta_value = maybe_unserialize($result->meta_value);
                $post_title = $result->post_title;
                $permalink = $result->guid;

                if (!in_array($post_id, $addedNodes)) {
                    $node = array(
                        'id' => $post_id,
                        'title' => $post_title,
                        'permalink' => $permalink,
                    );

                    $nodes[] = $node;
                    $addedNodes[] = $post_id;
                }

                foreach ($meta_value as $id => $linkedNode) {

                    if (!in_array($linkedNode['id'], $addedNodes)) {
                        $node = array(
                            'id' => (int)$linkedNode['id'],
                            'title' => $linkedNode['title'],
                            'permalink' => $linkedNode['permalink'],
                        );

                        $nodes[] = $node;
                        $addedNodes[] = $linkedNode['id'];
                    }

                    $links[] = array(
                        'target' => (int)$linkedNode['id'],
                        'source' => $post_id
                    );
                }
            }
        }

        return array(
            'nodes' => $nodes,
            'links' => $links
        );
    }

}

$graphViewForCustomRelatedPostsPlugin = GraphViewForCustomRelatedPostsPlugin::getInstance();